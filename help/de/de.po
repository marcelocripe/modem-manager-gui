# 
# Translators:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2013-2015
# Who Cares <transifex@redneptun.net>, 2020
msgid ""
msgstr ""
"Project-Id-Version: Modem Manager GUI\n"
"POT-Creation-Date: 2020-06-26 00:55+0300\n"
"PO-Revision-Date: 2020-06-25 22:01+0000\n"
"Last-Translator: Alex <alex@linuxonly.ru>\n"
"Language-Team: German (http://www.transifex.com/ethereal/modem-manager-gui/language/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Mario Blättermann <mario.blaettermann@gmail.com>"

#. (itstool) path: info/desc
#: C/about.page:8
msgid "Information about <app>Modem Manager GUI</app>."
msgstr "Informationen zu <app>Modem Manager GUI</app>."

#. (itstool) path: credit/name
#: C/about.page:12 C/contrib-code.page:12 C/contrib-translations.page:12
#: C/index.page:11 C/report-bugs.page:12 C/usage-config.page:12
#: C/usage-contacts.page:12 C/usage-getinfo.page:12 C/usage-modem.page:12
#: C/usage-netsearch.page:12 C/usage-sms.page:13 C/usage-traffic.page:12
#: C/usage-ussd.page:12
msgid "Mario Blättermann"
msgstr "Mario Blättermann"

#. (itstool) path: credit/name
#: C/about.page:16 C/contrib-code.page:16 C/index.page:15
#: C/report-bugs.page:16 C/usage-contacts.page:16 C/usage-netsearch.page:16
#: C/usage-sms.page:17 C/usage-traffic.page:16 C/usage-ussd.page:16
msgid "Alex"
msgstr "Alex"

#. (itstool) path: license/p
#: C/about.page:20 C/contrib-code.page:20 C/contrib-translations.page:16
#: C/index.page:19 C/report-bugs.page:20 C/usage-config.page:16
#: C/usage-contacts.page:20 C/usage-getinfo.page:16 C/usage-modem.page:16
#: C/usage-netsearch.page:20 C/usage-sms.page:21 C/usage-traffic.page:20
#: C/usage-ussd.page:20
msgid "Creative Commons Share Alike 3.0"
msgstr "Creative Commons Share Alike 3.0"

#. (itstool) path: page/title
#: C/about.page:24
msgid "About <app>Modem Manager GUI</app>"
msgstr "Info zu <app>Modem Manager GUI</app>"

#. (itstool) path: page/p
#: C/about.page:26
msgid ""
"<app>Modem Manager GUI</app> was written by <link "
"href=\"mailto:alex@linuxonly.ru\">Alex</link>. To find more information "
"about <app>Modem Manager GUI</app>, please visit the <link "
"href=\"https://linuxonly.ru/page/modem-manager-gui\"><app>Modem Manager "
"GUI</app> Web page</link>."
msgstr "<app>Modem Manager GUI</app> wurde von <link href=\"mailto:alex@linuxonly.ru\">Alex</link> entwickelt. Um mehr über <app>Modem Manager GUI</app> zu erfahren, besuchen Sie bitte die <link href=\"https://linuxonly.ru/page/modem-manager-gui\"><app>Modem Manager GUI</app>-Webseite</link>."

#. (itstool) path: page/p
#: C/about.page:34
msgid ""
"To report a bug or make a suggestion regarding this application or this "
"manual, use <link href=\"https://linuxonly.ru/forum/modem-manager-gui/\"> "
"<app>Modem Manager GUI</app> support forum</link>."
msgstr "Um einen Fehler zu melden oder einen Vorschlag für die Anwendung oder diese Anleitung zu machen, nutzen Sie bitte das <link href=\"https://linuxonly.ru/forum/modem-manager-gui/\"><app>Modem Manager GUI</app>-Supportforum</link>."

#. (itstool) path: page/p
#: C/about.page:40
msgid ""
"This program is distributed under the terms of the GNU General Public "
"license version 3, as published by the Free Software Foundation. A copy of "
"this license can be found at this <link href=\"help:gpl\">link</link>, or in"
" the file COPYING included with the source code of this program."
msgstr ""

#. (itstool) path: info/desc
#: C/contrib-code.page:8
msgid "How you can help make <app>Modem Manager GUI</app> better."
msgstr ""

#. (itstool) path: page/title
#: C/contrib-code.page:24
msgid "Provide code"
msgstr "Code beisteuern"

#. (itstool) path: page/p
#: C/contrib-code.page:26
msgid ""
"<app>Modem Manager GUI</app> has a version control system at "
"SourceForge.net. You can clone the repository with the following command:"
msgstr ""

#. (itstool) path: page/p
#: C/contrib-code.page:31
msgid ""
"<cmd>hg clone http://hg.code.sf.net/p/modem-manager-gui/code modem-manager-"
"gui-code</cmd>"
msgstr ""

#. (itstool) path: page/p
#: C/contrib-code.page:35
msgid ""
"Note, this clone command doesn't give you write access to the repository."
msgstr "Beachten Sie, dass dieser Befehl zum Klonen keinen Schreibzugriff auf den Softwarebestand gewährt."

#. (itstool) path: page/p
#: C/contrib-code.page:39
msgid ""
"For general help on how SourceForge works, see the <link "
"href=\"https://sourceforge.net/p/forge/documentation/ToC/\">SourceForge "
"documentation</link>."
msgstr ""

#. (itstool) path: page/p
#: C/contrib-code.page:44
msgid ""
"<app>Modem Manager GUI</app> source code is stored inside Mercurial "
"repository, so you don't have to read Git tutorials; just make sure you know"
" basic Mercurial commands and able to work on SourceForge platform."
msgstr ""

#. (itstool) path: info/desc
#: C/contrib-translations.page:8
msgid "Translate <app>Modem Manager GUI</app> into your native language."
msgstr "<app>Modem Manager GUI</app> in Ihre eigene Sprache übersetzen."

#. (itstool) path: page/title
#: C/contrib-translations.page:20
msgid "Translations"
msgstr "Übersetzungen"

#. (itstool) path: page/p
#: C/contrib-translations.page:22
msgid ""
"The graphical user interface, the traditional man page and the Gnome-style "
"user manual of <app>Modem Manager GUI</app> can be translated into your "
"language."
msgstr "Die grafische Benutzeroberfläche, die traditionelle Unix-Handbuchseite und das Benutzerhandbuch im Gnome-Stil von <app>Modem Manager GUI</app> kann in Ihre Sprache übersetzt werden."

#. (itstool) path: page/p
#: C/contrib-translations.page:27
msgid ""
"There is a <link href=\"https://www.transifex.com/projects/p/modem-manager-"
"gui/\"> project page on Transifex</link> where existing translations are "
"hosted and also new ones can be provided."
msgstr "Es gibt eine <link href=\"https://www.transifex.com/projects/p/modem-manager-gui/\"> Projektseite bei Transifex</link>, wo vorhandene Übersetzungen verfügbar sind und auch neue hinzugefügt werden können."

#. (itstool) path: page/p
#: C/contrib-translations.page:33
msgid ""
"For general help on how Transifex works, see the <link "
"href=\"http://support.transifex.com/\">Transifex Help Desk</link>."
msgstr "Allgemeine Hilfe zur Funktionsweise von Transifex finden Sie im <link href=\"http://support.transifex.com/\">Transifex Help Desk</link>."

#. (itstool) path: note/p
#: C/contrib-translations.page:39
msgid ""
"For your work you should have a look at the rules and dictionaries of the "
"local <link href=\"https://l10n.gnome.org/teams/\">Gnome translation teams "
"</link>. Although <app>Modem Manager GUI</app> shouldn't be considered as "
"pure Gnome software, it will be often used in GTK based environments and "
"should match the conceptual world of such applications."
msgstr "Für Ihre Arbeit sollten Sie einen Blick auf die Regelwerke und Standardwörterbücher Ihres lokalen <link href=\"https://l10n.gnome.org/teams/\">Gnome-Übersetzungsteams</link> werfen. Obwohl <app>Modem Manager GUI</app> nicht unbedingt als reine Gnome-Software betrachtet werden sollte, wird es oft in GTK-basierten Umgebungen verwendet und sollte deshalb die Begriffswelt solcher Anwendungen widerspiegeln."

#. (itstool) path: info/desc
#: C/index.page:6
msgid "Help for Modem Manager GUI."
msgstr "Hilfe zu Modem Manager GUI."

#. (itstool) path: title/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/index.page:7
msgctxt "_"
msgid "external ref='figures/gnome-hello-logo.png' md5='__failed__'"
msgstr "external ref='figures/gnome-hello-logo.png' md5='__failed__'"

#. (itstool) path: info/title
#: C/index.page:8
msgctxt "text"
msgid "Modem Manager GUI"
msgstr "Modem Manager GUI"

#. (itstool) path: title/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/index.page:23
msgctxt "_"
msgid ""
"external ref='figures/modem-manager-gui-logo.png' "
"md5='ab42d66239c7ef08904a9a921834cea2'"
msgstr ""

#. (itstool) path: page/title
#: C/index.page:23
msgid ""
"<media type=\"image\" src=\"figures/modem-manager-gui-logo.png\"/>Modem "
"Manager GUI manual"
msgstr ""

#. (itstool) path: page/p
#: C/index.page:25
msgid ""
"<app>Modem Manager GUI</app> is a graphical frontend for the ModemManager "
"daemon which is able to control specific modem functions."
msgstr "<app>Modem Manager GUI</app> ist eine grafische Benutzeroberfläche für den ModemManager-Systemdienst, der spezifische Modemfunktionen steuern kann."

#. (itstool) path: page/p
#: C/index.page:30
msgid "You can use <app>Modem Manager GUI</app> for the following tasks:"
msgstr "Sie können mit <app>Modem Manager GUI</app> folgende Aufgaben ausführen:"

#. (itstool) path: item/p
#: C/index.page:36
msgid "Create and control mobile broadband connections"
msgstr "Mobile Breitbandverbindungen erstellen und verwalten"

#. (itstool) path: item/p
#: C/index.page:39
msgid "Send and receive SMS messages and store messages in database"
msgstr "SMS-Nachrichten senden, empfangen und Nachrichten in einer Datenbank speichern"

#. (itstool) path: item/p
#: C/index.page:42
msgid ""
"Initiate USSD requests and read answers (also using interactive sessions)"
msgstr "USSD-Anfragen starten und Rückmeldungen lesen (ebenfalls in interaktiven Sitzungen)"

#. (itstool) path: item/p
#: C/index.page:45
msgid ""
"View device information: operator name, device mode, IMEI, IMSI, signal "
"level"
msgstr "Geräteinformationen anzeigen: Anbietername, Gerätemodell, IMEI, IMSI, Signalstärke"

#. (itstool) path: item/p
#: C/index.page:48
msgid "Scan available mobile networks"
msgstr "Verfügbare Mobilfunknetze suchen"

#. (itstool) path: item/p
#: C/index.page:51
msgid "View mobile traffic statistics and set limits"
msgstr "Mobildatenverkehrstatistiken anzeigen und Grenzwerte festlegen"

#. (itstool) path: section/title
#: C/index.page:56
msgid "Usage"
msgstr "Benutzung"

#. (itstool) path: section/title
#: C/index.page:60
msgid "Contribute to the project"
msgstr "Beiträge zum Projekt"

#. (itstool) path: info/desc
#: C/license.page:8
msgid "Legal information."
msgstr "Rechtliche Hinweise."

#. (itstool) path: page/title
#: C/license.page:11
msgid "License"
msgstr "Lizenz"

#. (itstool) path: page/p
#: C/license.page:13
msgid ""
"This work is distributed under a CreativeCommons Attribution-Share Alike 3.0"
" Unported license."
msgstr "Dieses Werk wird unter einer »CreativeCommons Attribution-Share Alike 3.0 Unported license« verbreitet."

#. (itstool) path: page/p
#: C/license.page:22
msgid "You are free:"
msgstr "Es ist Ihnen gestattet:"

#. (itstool) path: item/title
#: C/license.page:28
msgid "<em>To share</em>"
msgstr "<em>Freizugeben</em>"

#. (itstool) path: item/p
#: C/license.page:29
msgid "To copy, distribute and transmit the work."
msgstr "Das Werk bzw. den Inhalt vervielfältigen, verbreiten und öffentlich zugänglich machen."

#. (itstool) path: item/title
#: C/license.page:32
msgid "<em>To remix</em>"
msgstr "<em>Änderungen vorzunehmen</em>"

#. (itstool) path: item/p
#: C/license.page:33
msgid "To adapt the work."
msgstr "Abwandlungen und Bearbeitungen des Werkes bzw. Inhaltes anzufertigen."

#. (itstool) path: page/p
#: C/license.page:37
msgid "Under the following conditions:"
msgstr "Unter den folgenden Bedingungen:"

#. (itstool) path: item/title
#: C/license.page:43
msgid "<em>Attribution</em>"
msgstr "<em>Weitergabe</em>"

#. (itstool) path: item/p
#: C/license.page:44
msgid ""
"You must attribute the work in the manner specified by the author or "
"licensor (but not in any way that suggests that they endorse you or your use"
" of the work)."
msgstr "Sie dürfen das Werk nur unter gleichen Bedingungen weitergeben, wie Sie vom Autor oder Lizenzgeber festgelegt wurden (aber nicht so, dass es wie Ihr Werk aussieht)."

#. (itstool) path: item/title
#: C/license.page:51
msgid "<em>Share Alike</em>"
msgstr "<em>Share Alike</em>"

#. (itstool) path: item/p
#: C/license.page:52
msgid ""
"If you alter, transform, or build upon this work, you may distribute the "
"resulting work only under the same, similar or a compatible license."
msgstr "Wenn Sie das lizenzierte Werk bzw. den lizenzierten Inhalt bearbeiten, abwandeln oder in anderer Weise erkennbar als Grundlage für eigenes Schaffen verwenden, dürfen Sie die daraufhin neu entstandenen Werke bzw. Inhalte nur unter Verwendung von Lizenzbedingungen weitergeben, die mit denen dieses Lizenzvertrages identisch, vergleichbar oder kompatibel sind."

#. (itstool) path: page/p
#: C/license.page:59
msgid ""
"For the full text of the license, see the <link "
"href=\"http://creativecommons.org/licenses/by-"
"sa/3.0/legalcode\">CreativeCommons website</link>, or read the full <link "
"href=\"http://creativecommons.org/licenses/by-sa/3.0/\">Commons Deed</link>."
msgstr "Den vollständigen Text der Lizenz finden sie auf der <link href=\"http://creativecommons.org/licenses/by-sa/3.0/legalcode\">CreativeCommons-Webseite</link>. Oder Sie können den vollständigen <link href=\"http://creativecommons.org/licenses/by-sa/3.0/\">Commons Deed</link> lesen."

#. (itstool) path: info/desc
#: C/report-bugs.page:8
msgid "Report bugs and request new features."
msgstr "Fehler melden und neue Funktionen anfragen."

#. (itstool) path: page/title
#: C/report-bugs.page:24
msgid "Report bugs"
msgstr "Fehler melden"

#. (itstool) path: page/p
#: C/report-bugs.page:26
msgid ""
"If you found a bug in <app>Modem Manager GUI</app>, you can use the <link "
"href=\"https://linuxonly.ru/forum/modem-manager-gui/\">support forum</link>."
msgstr "Sollten Sie einen Fehler in <app>Modem Manager GUI</app>finden, können Sie das <link href=\"https://linuxonly.ru/forum/modem-manager-gui/\">Supportforum</link> nutzen."

#. (itstool) path: page/p
#: C/report-bugs.page:31
msgid ""
"Before filing a new topic, please have a look at <link "
"href=\"https://linuxonly.ru/forum/modem-manager-gui/\"> the existing "
"ones</link> first. Maybe someone else has already encountered the same "
"problem? Then you might write your comments there."
msgstr "Bevor Sie ein neues Thema eröffnen, prüfen Sie bitte zuerst <link href=\"https://linuxonly.ru/forum/modem-manager-gui/\">die bereits bestehenden</link>. Vielleicht ist jemand bereits auf dasselbe Problem gestoßen? Dann könnten Sie Ihre Kommentare dort hinterlassen."

#. (itstool) path: note/p
#: C/report-bugs.page:38
msgid "You can also use the support forum for feature requests."
msgstr "Sie können das Supportforum nutzen, um neue Programmfeatures vorzuschlagen."

#. (itstool) path: info/desc
#: C/usage-config.page:8
msgid "Configure the application to match your needs."
msgstr "Anpassung dieser Anwendung an Ihre Wünsche und Erfordernisse."

#. (itstool) path: page/title
#: C/usage-config.page:20
msgid "Configuration"
msgstr "Konfiguration"

#. (itstool) path: info/desc
#: C/usage-contacts.page:8
msgid "Use your contact lists."
msgstr "Ihre Kontaktlisten verwenden."

#. (itstool) path: page/title
#: C/usage-contacts.page:24
msgid "Contact lists"
msgstr "Kontaktlisten"

#. (itstool) path: page/p
#: C/usage-contacts.page:26
msgid ""
"Broadband modem has access to contacts stored on SIM card. Some modems can "
"also store contacts in internal memory. <app>Modem Manager GUI</app> can "
"work with contacts from these storages and also can export contacts from "
"system contacts storages. Supported system contact storages are: Evolution "
"Data Server used by GNOME applications (GNOME contacts section) and Akonadi "
"server used by KDE applications (KDE contacts section)."
msgstr "Das Modem kann auf die in der SIM-Karte gespeicherten Kontakte zugreifen. Einige Modems speichern auch Kontakte im internen Speicher. <app>Modem Manager GUI</app> kann die Kontakte in diesen Speicherbereichen verarbeiten und auch Kontakte aus dem System exportieren. Dabei werden der von Gnome-Anwendungen genutzte Evolution Data Server (Gnome-Kontakte) und der von KDE-Anwendungen verwendete Akonadi-Server (KDE-Kontakte) unterstützt."

#. (itstool) path: figure/title
#: C/usage-contacts.page:35
msgid "Contacts window of <app>Modem Manager GUI</app>."
msgstr ""

#. (itstool) path: figure/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/usage-contacts.page:36
msgctxt "_"
msgid ""
"external ref='figures/contacts-window.png' "
"md5='d5d3b2a8dd0db7eacf7dd9f80950cfc7'"
msgstr "external ref='figures/contacts-window.png' md5='d5d3b2a8dd0db7eacf7dd9f80950cfc7'"

#. (itstool) path: page/p
#: C/usage-contacts.page:39
msgid ""
"Use <gui style=\"button\">New contact</gui> button to fill form and add new "
"contact to SIM card or modem storage, <gui style=\"button\">Remove "
"contact</gui> button to remove selected contact from SIM card or modem "
"storage and <gui style=\"button\">Send SMS</gui> button to compose and send "
"SMS message to selected contact (either from SIM card or modem, or exported "
"from system contacts storage)."
msgstr "Nutzen Sie die <gui style=\"button\">Neuer Kontakt</gui>-Schaltfläche um ein Formular auszufüllen und einen neuen Kontakt im SIM-Karten- oder Modemspeicher anzulegen. Nutzen Sie die <gui style=\"button\">Kontakt entfernen</gui>-Schaltfläche, um den gewählten Kontakt aus dem SIM-Karten- oder Modemspeicher zu löschen. Nutzen Sie die <gui style=\"button\">SMS senden</gui>-Schaltfläche um SMS zu verfassen und an den gewählten Kontakt (entweder aus dem SIM-Karten- oder Modemspeicher, oder aus einem Systemkontakte-Export stammend) zu senden."

#. (itstool) path: note/p
#: C/usage-contacts.page:47
msgid ""
"Some backends don't support modem contacts manipulation functions. For "
"example, ModemManager doesn't work with contacts from SIM card and modem "
"memory at all (this functionality isn't implemented yet), and oFono can only"
" export contacts from SIM card (this is developer's decision)."
msgstr "Manche Backends unterstützen die Manipulation von Modemkontakten nicht. Zum Beispiel funktioniert ModemManager überhaupt nicht mit Kontakten aus dem SIM-Karten oder Modemspeicher (diese Funktionalität wurde bisher noch nicht implementiert) und oFono kann nur Kontakte von der SIM-Karte exportieren (hierbei handelt sich dabei um eine Entwicklerentscheidung)."

#. (itstool) path: info/desc
#: C/usage-getinfo.page:8
msgid "Get info about the mobile network."
msgstr "Informationen zum Mobilfunknetzwerk abrufen."

#. (itstool) path: page/title
#: C/usage-getinfo.page:20
msgid "Network info"
msgstr "Netzwerk-Info"

#. (itstool) path: page/p
#: C/usage-getinfo.page:22
msgid ""
"Your network operator provides some info which you can view in <app>Modem "
"Manager GUI</app>. Click on the <gui style=\"button\">Info</gui> button in "
"the toolbar."
msgstr "Ihr Netzanbieter stellt einige Informationen bereit, die Sie in <app>Modem Manager GUI</app> betrachten können. Klicken Sie auf den <gui style=\"button\">Info</gui>-Knopf in der Werkzeugleiste."

#. (itstool) path: page/p
#: C/usage-getinfo.page:28
msgid ""
"In the following window you see all available information as provided from "
"your operator:"
msgstr "Im folgenden Fenster sehen Sie alle Informationen, so wie sie von Ihrem Netzanbieter bereitgestellt werden:"

#. (itstool) path: figure/title
#: C/usage-getinfo.page:34
msgid "Network information window of <app>Modem Manager GUI</app>."
msgstr ""

#. (itstool) path: figure/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/usage-getinfo.page:35
msgctxt "_"
msgid ""
"external ref='figures/network-info.png' "
"md5='46b945ab670dabf253f73548c0e6b1a0'"
msgstr "external ref='figures/network-info.png' md5='46b945ab670dabf253f73548c0e6b1a0'"

#. (itstool) path: page/p
#: C/usage-getinfo.page:38
msgid ""
"The most informations are self-explained and well known from traditional "
"mobile phones or smartphones. Note, the GPS based location detection (in the"
" lower part of the window) won't work in most cases because mobile broadband"
" devices usually don't have a GPS sensor."
msgstr "Die meisten Informationen sind selbsterklärend und von traditionellen Mobiltelefonen oder Smartphones bekannt. Beachten Sie, dass die GPS-basierte Ortung (im unteren Teil des Fensters) in den meisten Fällen nicht funktioniert, da mobile Breitbandgeräte üblicherweise nicht über einen GPS-Sensor verfügen."

#. (itstool) path: info/desc
#: C/usage-modem.page:8
msgid "Activate your modem devices."
msgstr "Aktivieren Ihrer Modem-Geräte."

#. (itstool) path: page/title
#: C/usage-modem.page:20
msgid "Modems"
msgstr "Modems"

#. (itstool) path: page/p
#: C/usage-modem.page:22
msgid ""
"After starting <app>Modem Manager GUI</app>, the following window will be "
"displayed:"
msgstr "Nach dem Start von <app>Modem Manager GUI</app> wird das folgende Fenster angezeigt:"

#. (itstool) path: figure/title
#: C/usage-modem.page:27
msgid "The startup window of <app>Modem Manager GUI</app>."
msgstr ""

#. (itstool) path: figure/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/usage-modem.page:28
msgctxt "_"
msgid ""
"external ref='figures/startup-window.png' "
"md5='0732138275656f836e2c0f2905b715fd'"
msgstr "external ref='figures/startup-window.png' md5='0732138275656f836e2c0f2905b715fd'"

#. (itstool) path: page/p
#: C/usage-modem.page:31
msgid ""
"You can see the modem devices available on your system. Click on one of the "
"entries to use that device."
msgstr "Sie können die Modems sehen, die in Ihrem System verfügbar sind. Klicken Sie auf einen der Einträge, um dieses Gerät zu verwenden."

#. (itstool) path: page/p
#: C/usage-modem.page:36
msgid ""
"After clicking on a device, it might be needed to activate it first, if it "
"was not otherwise activated on your system. <app>Modem Manager GUI</app> "
"will ask you for confirmation in that case."
msgstr "Nach dem Anklicken eines Gerätes kann es notwendig sein, es zuerst zu aktivieren, sofern es noch nicht anderweitig auf Ihrem System aktiviert wurde. <app>Modem Manager GUI</app> bittet Sie in diesem Fall um Bestätigung."

#. (itstool) path: page/p
#: C/usage-modem.page:42
msgid ""
"Be patient after connecting a removable device such as an USB stick or "
"PCMCIA card. It may take a while until the system detects it."
msgstr "Haben Sie Geduld, wenn Sie ein externes Gerät wie beispielsweise einen USB-Stick oder eine PCMCIA-Karte eingesteckt haben. Es kann etwas dauern, bis das System es erkennt."

#. (itstool) path: note/p
#: C/usage-modem.page:48
msgid ""
"You cannot use multiple modems at the same time. If you click on another "
"entry in the device list, the previously activated one will be disabled."
msgstr "Sie können nicht mehrere Modems gleichzeitig verwenden. Wenn Sie auf einen anderen Eintrag in der Geräteliste klicken, wird das vorher aktivierte Gerät deaktiviert."

#. (itstool) path: info/desc
#: C/usage-netsearch.page:8
msgid "Search for available networks."
msgstr "Nach verfügbaren Mobilfunknetzwerken suchen."

#. (itstool) path: page/title
#: C/usage-netsearch.page:24
msgid "Network Search"
msgstr "Netzwerksuche"

#. (itstool) path: page/p
#: C/usage-netsearch.page:26
msgid ""
"<app>Modem Manager GUI</app> can be used to search available mobile "
"networks. Click on the <gui style=\"button\">Scan</gui> button in the "
"toolbar."
msgstr "Mit <app>Modem Manager GUI</app> können Sie nach verfügbaren Mobilfunknetzwerken suchen. Klicken Sie hierzu auf den <gui style=\"button\">Suchen</gui>-Knopf in der Werkzeugleiste."

#. (itstool) path: figure/title
#: C/usage-netsearch.page:32
msgid "Network search window of <app>Modem Manager GUI</app>."
msgstr ""

#. (itstool) path: figure/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/usage-netsearch.page:33
msgctxt "_"
msgid ""
"external ref='figures/scan-window.png' "
"md5='8a9dd66ef917dcd42de0d8a69fb84ccd'"
msgstr "external ref='figures/scan-window.png' md5='8a9dd66ef917dcd42de0d8a69fb84ccd'"

#. (itstool) path: page/p
#: C/usage-netsearch.page:36
msgid ""
"If one mobile network appears more than once in the list, this mobile "
"network use different broadcasting standards. It is obvioius that you can't "
"scan mobile networks using broadcasting standards that your modem doesn't "
"support."
msgstr "Falls ein Mobilfunknetzwerk mehrfach in der Liste erscheint, verwendet dieses Netzwerk verschiedene Broadcasting-Standards. Sie können keine Mobilfunknetzwerke nutzen, deren Broadcasting-Standards von Ihrem Modem nicht unterstützt werden."

#. (itstool) path: info/desc
#: C/usage-sms.page:9
msgid "Use <app>Modem Manager GUI</app> for sending and receiving SMS."
msgstr "<app>Modem Manager GUI</app> zum Senden und Empfangen von SMS verwenden."

#. (itstool) path: page/title
#: C/usage-sms.page:25
msgid "SMS"
msgstr "SMS"

#. (itstool) path: page/p
#: C/usage-sms.page:27
msgid ""
"Most broadband modems able to send and receive SMS messages just like any "
"mobile phone. You can use <app>Modem Manager GUI</app> if you want to send "
"or read received SMS messages. Click on the <gui style=\"button\">SMS</gui> "
"button in the toolbar."
msgstr "Die meisten Modems können SMS-Nachrichten senden und empfangen, wie jedes gewöhnliche Mobiltelefon. Auch mit <app>Modem Manager GUI</app> können Sie SMS versenden oder empfangene Nachrichten lesen. Klicken Sie dazu auf den <gui style=\"button\">SMS</gui>-Knopf in der Werkzeugleiste."

#. (itstool) path: page/p
#: C/usage-sms.page:34
msgid ""
"As you can see, all messages are stored in three folders. You can find "
"received messages in the Incoming folder, sent messages in the Sent folder "
"and saved messages in the Drafts folder."
msgstr "Alle Nachrichten werden in drei Ordnern sortiert angezeigt. Empfangene Nachrichten befinden sich im Eingang-Ordner angezeigt, gesendete Nachrichten im Gesendet-Ordner und gespeicherte Nachrichten im Entwürfe-Ordner."

#. (itstool) path: figure/title
#: C/usage-sms.page:41
msgid "SMS window of <app>Modem Manager GUI</app>."
msgstr ""

#. (itstool) path: figure/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/usage-sms.page:42
msgctxt "_"
msgid ""
"external ref='figures/sms-window.png' md5='7032b1a7fff2f2834ea821cf8eeb14ac'"
msgstr "external ref='figures/sms-window.png' md5='7032b1a7fff2f2834ea821cf8eeb14ac'"

#. (itstool) path: page/p
#: C/usage-sms.page:45
msgid ""
"You can tweak messages sorting order and SMS special parameters using "
"Preferences window."
msgstr "Die Sortierreihenfolge der Nachrichten sowie einige spezielle SMS-Parameter können Sie im Einstellungsfenster ändern."

#. (itstool) path: page/p
#: C/usage-sms.page:50
msgid ""
"Use <gui style=\"button\">New</gui> button to compose and send or save "
"message, <gui style=\"button\">Remove</gui> button to remove selected "
"message(s) and <gui style=\"button\">Answer</gui> button to answer selected "
"message (if this message has valid number). Don't forget that you can select"
" more than one message at once."
msgstr "Mit dem <gui style=\"button\">Neu</gui> können Sie Nachrichten schreiben und anschließend versenden oder speichern, mit dem <gui style=\"button\">Entfernen</gui>-Knopf löschen Sie die ausgewählte(n) Nachricht(en) und mit dem <gui style=\"button\">Antworten</gui>-Knopf beantworten Sie die ausgewählte Nachricht (sofern diese Nachricht eine gültige Rufnummer hat). Sie können dabei nicht mehrere Nachrichten gleichzeitig auswählen."

#. (itstool) path: info/desc
#: C/usage-traffic.page:8
msgid "Get statistics about network traffic."
msgstr "Statistiken zum Netzwerkverkehr abrufen."

#. (itstool) path: page/title
#: C/usage-traffic.page:24
msgid "Network traffic"
msgstr "Netzwerkverkehr"

#. (itstool) path: page/p
#: C/usage-traffic.page:26
msgid ""
"<app>Modem Manager GUI</app> collects mobile broadband traffic statistics "
"and able to disconnect modem when traffic consumption or time of the session"
" exceeds user-defined limit. To use such functions, click on the <gui "
"style=\"button\">Traffic</gui> button in the toolbar."
msgstr "<app>Modem Manager GUI</app> ermittelt Statistiken zum Datenverkehr im Mobilfunknetz und kann das Modem trennen, sobald die Datenmenge oder die Zeit eine vom Benutzer definierte Begrenzung übersteigt. Klicken Sie hierzu auf den <gui style=\"button\">Netzwerkverkehr</gui>-Knopf in der Werkzeugleiste."

#. (itstool) path: page/p
#: C/usage-traffic.page:32
msgid ""
"Network traffic window has list with session, month and year traffic "
"consumption information on the left and graphical representation of current "
"network connection speed on the right."
msgstr "Im Netzwerkverkehr-Fenster finden Sie auf der linken Seite Listen mit dem sitzungsbezogenen, monatlichen und jährlichen Netzwerkverkehr, sowie auf der rechten Seite eine grafische Darstellung der aktuellen Übertragungsgeschwindigkeit."

#. (itstool) path: figure/title
#: C/usage-traffic.page:38
msgid "Network traffic window of <app>Modem Manager GUI</app>."
msgstr ""

#. (itstool) path: figure/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/usage-traffic.page:39
msgctxt "_"
msgid ""
"external ref='figures/traffic-window.png' "
"md5='3cced75039a5230c68834ba4aebabcc3'"
msgstr "external ref='figures/traffic-window.png' md5='3cced75039a5230c68834ba4aebabcc3'"

#. (itstool) path: page/p
#: C/usage-traffic.page:42
msgid ""
"Use <gui style=\"button\">Set limit</gui> button to set traffic or time "
"limit for current session, <gui style=\"button\">Connections</gui> button to"
" view list of active network connections and terminate applications "
"consuming network traffic and <gui style=\"button\">Statistics</gui> button "
"to view daily traffic consumption statistics for selected months."
msgstr "Mit dem Knopf <gui style=\"button\">Begrenzung setzen</gui> begrenzen Sie den Netzwerkverkehr für die aktuelle Sitzung, enweder basierend auf der Datenmenge oder der Zeit. Mit dem Knopf <gui style=\"button\">Verbindungen</gui> erhalten Sie eine Liste der aktiven Netzwerkverbindungen und beenden Anwendungen, die Netzwerkressourcen beanspruchen. In den <gui style=\"button\">Statistiken</gui> wird der tägliche Netzwerkverkehr in den ausgewählten Monaten angezeigt."

#. (itstool) path: note/p
#: C/usage-traffic.page:50
msgid ""
"Traffic statistics are collected only while <app>Modem Manager GUI</app> is "
"running, so result values can be inaccurate and must be treated as reference"
" only. The most accurate traffic statistics are collected by mobile "
"operator."
msgstr "Netzwerkstatistiken werden nur ermittelt, während <app>Modem Manager GUI</app> läuft. Daher können die sich ergebenden Werte ungenau sein und sollten nur als Referenz betrachtet werden. Die genauesten Werte ermittelt Ihr Mobilfunkanbieter."

#. (itstool) path: info/desc
#: C/usage-ussd.page:8
msgid ""
"Use <app>Modem Manager GUI</app> for send USSD codes and receive the "
"answers."
msgstr "<app>Modem Manager GUI</app> zum Senden von USSD-Codes und Empfangen der Antworten verwenden."

#. (itstool) path: page/title
#: C/usage-ussd.page:24
msgid "USSD codes"
msgstr "USSD-Codes"

#. (itstool) path: page/p
#: C/usage-ussd.page:26
msgid ""
"<app>Modem Manager GUI</app> is able to send USSD codes. These codes are "
"controlling some network functions, for example the visibility of your phone"
" number when sending a SMS."
msgstr "<app>Modem Manager GUI</app> bietet die Möglichkeit, USSD-Codes zu senden. Diese Codes steuern Netzwerkfunktionen, zum Beispiel die Sichtbarkeit Ihrer Telefonnummer beim Senden einer SMS-Nachricht."

#. (itstool) path: page/p
#: C/usage-ussd.page:31
msgid ""
"To use the USSD functions, click on the <gui style=\"button\">USSD</gui> "
"button in the toolbar."
msgstr "Um die USSD-Funktionen zu verwenden, klicken Sie auf den Knopf <gui style=\"button\">USSD</gui> in der Werkzeugleiste."

#. (itstool) path: figure/title
#: C/usage-ussd.page:37
msgid "USSD window of <app>Modem Manager GUI</app>."
msgstr ""

#. (itstool) path: figure/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/usage-ussd.page:38
msgctxt "_"
msgid ""
"external ref='figures/ussd-window.png' "
"md5='c1f2437ed53d67408c1fdfeb270f001a'"
msgstr "external ref='figures/ussd-window.png' md5='c1f2437ed53d67408c1fdfeb270f001a'"

#. (itstool) path: page/p
#: C/usage-ussd.page:41
msgid ""
"In the text entry on top of the window, the code <cmd>*100#</cmd> is already"
" displayed. This code is the usual one for requesting the balance for a "
"prepaid card. If you like to send another code, click on the <gui "
"style=\"button\">Edit</gui> button on the right."
msgstr ""

#. (itstool) path: page/p
#: C/usage-ussd.page:47
msgid ""
"<app>Modem Manager GUI</app> supports interactive USSD sessions, so pay "
"attention to hints displayed under USSD answers. You can send USSD responses"
" using text entry for USSD commands. If you'll send new USSD command while "
"USSD session is active, this session will be closed automatically."
msgstr "<app>Modem Manager GUI</app> unterstützt interaktive USSD-Sitzungen, daher sollten Sie auf die in den USSD-Antworten enthaltenen Hinweise achten. Anworten auf USSD-Befehle können Sie in die Texteingabezeile schreiben und absenden. Wenn Sie einen neuen USSD-Befehl senden, während eine Sitzung aktiv ist, wird diese Sitzung automatisch geschlossen."

#. (itstool) path: note/p
#: C/usage-ussd.page:54
msgid ""
"If you use Huawei device and get unreadable USSD answers, you can try to "
"click on the <gui style=\"button\">Edit</gui> button and toggle the <gui "
"style=\"button\">Change message encoding</gui> button in the toolbar of "
"opened window."
msgstr "Wenn Sie ein Huawei-Gerät nutzen und unlesbare USSD-Antworten erhalten, können Sie eventuell folgendermaßen Abhilfe schaffen: Klicken Sie auf den <gui style=\"button\">Bearbeiten</gui>-Knopf und aktivieren Sie die Option <gui style=\"button\">Zeichenkodierung der Nachricht ändern</gui>."

#. (itstool) path: note/p
#: C/usage-ussd.page:62
msgid ""
"USSD codes are only available in networks which use the <link "
"href=\"http://en.wikipedia.org/wiki/3rd_Generation_Partnership_Project\">3GPP"
" standards</link>."
msgstr ""

#. (itstool) path: page/p
#: C/usage-ussd.page:68
msgid ""
"You can use such codes for many purposes: change plan, request balance, "
"block number, etc."
msgstr "Sie können solche Codes für viele Zwecke verwenden: Tarif ändern, Guthaben aufladen, Telefonnummern blockieren usw."
